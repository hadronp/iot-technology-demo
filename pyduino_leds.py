#!/usr/bin/env python

from Arduino import Arduino
import time

board = Arduino('9600') 
led_pins= [2,3,4,5,6,7,8,9]

#setup
for pin in led_pins:
	board.pinMode(pin,"OUTPUT")
	print "Configured pin " + str(pin) + " as OUTPUT"

#loop
while True:
	pin=0
	for pin in led_pins:
		board.digitalWrite(pin, "HIGH") 
		print "LED on pin " + str(pin) # confirm HIGH (1)
		time.sleep(0.4)

		board.digitalWrite(pin, "LOW")
		print "LED off pin " + str(pin) # confirm LOW (0)
		time.sleep(0.4)
'''
	for pin in reversed(led_pins):
		board.digitalWrite(pin, "HIGH") 
		print "LED on"# confirm HIGH (1)
		time.sleep(0.03)

		board.digitalWrite(pin, "LOW")
		print "LED off"# confirm LOW (0)
		time.sleep(0.03)
'''