#!/usr/bin/env python

from Arduino import Arduino
import time

led_pin=13

board = Arduino('9600') #plugged in via USB, serial com at rate 9600, port auto detected, dmesg | grep tty for manual port
board.pinMode(led_pin, "OUTPUT")

while True:
	board.digitalWrite(led_pin, "LOW")
	print "LED off"# confirm LOW (0)
	time.sleep(1)
	
	board.digitalWrite(led_pin, "HIGH") 
	print "LED on"# confirm HIGH (1)
	time.sleep(1)