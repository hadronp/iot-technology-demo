import serial
import xively
import datetime

# set api key
api=xively.XivelyAPIClient("9zlrz8WKkraBRlnTlon0PAp6avXFrD7CTJ1zEb9vbqSO0YQE")
print api

# set feed id
feed = api.feeds.get(1958624096)
print feed
print list(feed.datastreams)

# get data stream objects
light_sensor_ds = feed.datastreams.get('light_sensor')
temp_sensor_ds = feed.datastreams.get('temp_sensor')

print  light_sensor_ds
print temp_sensor_ds

ser= serial.Serial('/dev/ttyACM0',9600, timeout=2.0)
print ser

# the last time we sent to the stream
lastSend = None

while True:	
	
	data = ser.readline().strip()
	now = datetime.datetime.utcnow()

	end_of_data = data.index('#')
	payload = data[:end_of_data]
	delimiter= payload.index('%')

	temp_data = payload[ :delimiter ]
	light_data = payload[ delimiter + 1: len(payload) ]

	if lastSend is None or now - lastSend > datetime.timedelta( seconds = 3 ):
	    td = float(temp_data)
	    ld = float(light_data)
	    
	    print "sending temp data %.2f at %s" % (td, now.strftime( "%Y-%m-%d %H:%M:%S" ))
	    print "sending light data %.2f at %s" % (ld, now.strftime( "%Y-%m-%d %H:%M:%S" ))
	    
	    temp_sensor_ds.current_value = td
	    light_sensor_ds.current_value = ld

	    temp_sensor_ds.at = now
	    light_sensor_ds.at = now

	    temp_sensor_ds.update()
	    light_sensor_ds.update()

	    lastSend=now

ser.close()   