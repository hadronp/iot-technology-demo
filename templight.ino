float tempC;
float tempF;
int lightLevel;

int tempPin = 1;
int lightPin=0;
//int ledPin=13;

void setup()
{
  Serial.begin(9600);
  //pinMode(ledPin, OUTPUT);
}

void loop()
{
  tempC = analogRead(tempPin);    
  lightLevel=analogRead(lightPin);
  
  // map light values from 0-1024 to 0-255 bits of resolution
  int ll=map(lightLevel,0,1024,0,255);
  
  tempC = (5.0 * tempC * 100.0)/1024.0;  //convert the analog data to temperature
  
  /**if(ll< 90){
    digitalWrite(ledPin, HIGH);
  }
  else{
    digitalWrite(ledPin, LOW);
  }
  **/
  
  Serial.print(tempC);
  Serial.print("%");
  Serial.print(ll);
  Serial.println("#");
  delay(1000);                           
}