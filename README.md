This is a simple demonstration of IoT concepts using the following technologies:

Arduino + LM35 Temperature sensor, LDR for light sensor

Serial transmission of data from Arduino to Xively cloud platform
is done via a Python proxy script and Python Xively Library

Two (2) realtime data streams for temperature and light data were setup in Xively and is accessed via a REST endpoint and API key

Frontend presentation via HTML5, Javascript, AngularJS and Xively Library

