#!/usr/bin/env python

from Arduino import Arduino
import xively
import datetime
import time

# Xively setup
# set api key
api=xively.XivelyAPIClient("9zlrz8WKkraBRlnTlon0PAp6avXFrD7CTJ1zEb9vbqSO0YQE")
print api

# set feed id
feed = api.feeds.get(1958624096)
print feed
print list(feed.datastreams)

# get data stream objects
light_sensor_ds = feed.datastreams.get('light_sensor')
temp_sensor_ds = feed.datastreams.get('temp_sensor')

# display data stream objects
print  light_sensor_ds
print temp_sensor_ds


# Python-Arduino API setup
# Instantiate Arduino board object at 9600 baud port auto-detect
# Analog pin 0- light sensor (ldr) 
# Analog pin 1- temperature sensor (LM35)
board = Arduino('9600')

# the last time we sent to the stream
lastSend = None

while True:
	now = datetime.datetime.utcnow()

	time.sleep(1) #throttle read rate from analog pins by 1 second
	light_data = board.analogRead(0)
	light_data_c = light_data / 4 # set range 0-255
	
	temp_data = board.analogRead(1)
	temp_data_c= (5.0 * temp_data * 100.0)/1024.0 #convert to degree celsius
	
	if lastSend is None or now - lastSend > datetime.timedelta( seconds = 3 ):

	    print "sending temp data %.2f at %s" % (temp_data_c, now.strftime( "%Y-%m-%d %H:%M:%S" ))
	    print "sending light data %.2f at %s" % (light_data_c, now.strftime( "%Y-%m-%d %H:%M:%S" ))
	    
	    temp_sensor_ds.current_value = "{0:.2f}".format(temp_data_c)
	    light_sensor_ds.current_value = "{0:.2f}".format(light_data_c)

	    temp_sensor_ds.at = now
	    light_sensor_ds.at = now

	    temp_sensor_ds.update()
	    light_sensor_ds.update()

	    lastSend=now

board.close()   