(function () {
    'use strict';

    xively.setKey("RwoHJb1EwsU2nnc4yZazGK76i3l4J7fFTTcT4jA7j6B87ydL");

    angular.module('xApp', ['angularCharts']);

    angular.module('xApp').controller('MainCtrl', MainCtrl);

    function MainCtrl($scope) {
        $scope.feed_id = "1958624096";

        $scope.datastreams = {
            light_sensor: '0',
            temp_sensor: '0'
        };

        $scope.config = {
            title: 'XDataReader',
            tooltips: false,
            labels: false,
            mouseover: function () {
            },
            mouseout: function () {
            },
            click: function () {
            },
            legend: {
                display: true,
                position: 'left'
            },
            innerRadium: 0,
            lineLegend: 'lineEnd'
        };

        $scope.data = {
            series: ['Brightness', 'Temperature'],
            data: [
                { x: "0:00", y: [0, 0]},
                { x: "0:01", y: [256, 256]}
            ]
        };

        function timeNow() {
            var d = new Date(); // for now
            var hour = d.getHours();
            var min = d.getMinutes();
            var sec = d.getSeconds();
            var timeNow = hour + ":" + min + ":" + sec;
            return timeNow;
        }

        $scope.$watch('datastreams.light_sensor', function () {
            var chartData = {   x: timeNow(),
                y: [$scope.datastreams.light_sensor, $scope.datastreams.temp_sensor] };
            $scope.data.data.push(chartData);
        });


        angular.forEach(Object.keys($scope.datastreams), function (ds) {
            //for ds in Object.keys($scope.datastreams) {
            xively.datastream.get($scope.feed_id, ds, function (data) {
                $scope.$apply(function () {
                    $scope.datastreams[ds] = data.current_value;
                });
                xively.datastream.subscribe($scope.feed_id, ds, function (event, data) {
                    $scope.$apply(function () {
                        $scope.datastreams[ds] = data.current_value;
                    });
                });
            });
        });
    }
});

